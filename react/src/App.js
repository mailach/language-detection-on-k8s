import logo from "./logo.svg";
import "./App.css";
import { LanguageDetection } from "./LanguageDetection";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <LanguageDetection />
      </header>
    </div>
  );
}

export default App;
