import React, { useState } from "react";
import { Button, Card, Box, Typography } from "@mui/material";

const fastapi_domain = "guess-this-lang.org/fastapi";

export const FeedbackBox = (text) => {
  const [feedbackSent, setFeedbackSent] = useState("");

  const apiPost = (label) => {
    const data = {
      label: label,
      ...text,
    };

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    };

    fetch(`http://${fastapi_domain}/postfeedback/`, requestOptions)
      .then((response) => response.json())
      .then((json) => {
        if (json === "Posted feedback") {
          setFeedbackSent(true);
        }
      });
  };

  return (
    <Card>
      <Box sx={{ flexDirection: "column", padding: "8px" }}>
        {!feedbackSent ? (
          <>
            <Typography>Not correct? Choose the right label: </Typography>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                variant="outlined"
                size="small"
                onClick={() => apiPost("german")}
              >
                German
              </Button>
              <Button
                variant="outlined"
                size="small"
                onClick={() => apiPost("english")}
              >
                English
              </Button>
              <Button
                variant="outlined"
                size="small"
                onClick={() => apiPost("spanish")}
              >
                Spanish
              </Button>
            </Box>
          </>
        ) : (
          <Typography>Thank you for the feedback!</Typography>
        )}
      </Box>
    </Card>
  );
};
