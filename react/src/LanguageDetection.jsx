import React, { useState } from "react";
import { Button, Card, Box, TextField } from "@mui/material";
import { FeedbackBox } from "./FeedbackBox";

const fastapi_domain = "mlflow.sws.informatik.uni-leipzig.de/fastapi"; //likely needs to be changed, check your minikube ip by running "minikube ip"

export const LanguageDetection = () => {
  const [text, setText] = useState("");
  const [apiResponse, setApiResponse] = useState("");
  const [sentText, setSentText] = useState("");

  const apiGet = () => {
    fetch(`http://${fastapi_domain}/detect_lang/?text=${text}`)
      .then((response) => response.json())
      .then((json) => {
        setApiResponse(json);
        setSentText(text);
      });
  };

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "12px" }}>
      <Card>
        <Box
          sx={{ flexDirection: "column", paddingBottom: "4px", margin: "20px" }}
        >
          <TextField
            sx={{ height: "150px", width: "400px" }}
            id="tf"
            label="Please enter your text"
            multiline
            rows={4}
            onChange={(e) => setText(e.target.value)}
          />
          <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
            <Button variant="contained" onClick={apiGet}>
              Detect language
            </Button>
          </Box>
        </Box>
      </Card>
      {apiResponse !== "" && sentText === text && (
        <>
          <Card>
            Your text is <span style={{ color: "#1976d2" }}>{apiResponse}</span>
          </Card>
          <FeedbackBox text={sentText} />
        </>
      )}
    </Box>
  );
  // Oh my, if only I used TS
};
