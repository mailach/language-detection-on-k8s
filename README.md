# Language Detection on Minikube

A simple application consisting of a FastAPI app that serves the model (modularization code from https://github.com/SirBenedick/se4ai-p01) and a React App. While other branches are targeting the deployment on minikube and the explanation of different k8s components, this branch is dedicated to connect to the sws on-prem k8s-cluster. You'll need to have access to a namespace to connect.

## Configure connection to K8s

:warning: **You have to be connected to the university VPN**

1. Install krew by executing in your shell:

   ```
   (
       set -x; cd "$(mktemp -d)" &&
       OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
       ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
       KREW="krew-${OS}_${ARCH}" &&
       curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
       tar zxvf "${KREW}.tar.gz" &&
       ./"${KREW}" install krew
   )
   ```

2. Add .krew/bin to your PATH

   ```
   export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
   ```

   Then restart the shell

3. Install necessary plugins

   ```
   (
       kubectl krew update
       kubectl krew upgrade
       kubectl krew install ns ctx oidc-login
   )
   ```

4. Copy the content of `kubeconfig` to `.kube/config` with the correct `chmod 600` permissions. Run `kubectl get pods -n your-namespace`, your browser will automatically direct you to login to gitlab for authentication.

## Deploy application (state of '02-minikube-with-ingress')

```bash
kubectl apply -f .
```

## Delete application

```bash
kubectl delete -f .
```
