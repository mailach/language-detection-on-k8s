from fastapi import FastAPI

from fastapi.middleware.cors import CORSMiddleware
from language_detection.lang_detector import LanguageDetector

subpath = '/fastapi'

app = FastAPI(docs_url=f'{subpath}/docs', openapi_url=f'{subpath}/openapi.json')

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


detector = LanguageDetector('data/trained_models/simple_mlp_novectorize.h5', 'data/trained_models/vectorizer')

@app.get(f'{subpath}/')
def get_root():
  return {'message': 'Welcome to the language detection API'}

@app.get(f'{subpath}/detect_lang/')
async def detect_language(text: str):
	return detector.detect_language(text)
